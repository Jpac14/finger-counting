import cv2
import mediapipe as mp
import numpy as np

mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_hands = mp.solutions.hands

cap = cv2.VideoCapture(0)
with mp_hands.Hands(
    model_complexity=0,
    min_detection_confidence=0.5,
    min_tracking_confidence=0.5
) as hands:
    while cap.isOpened():
        success, image = cap.read()
        if not success:
            print("Ignoring empty camera frame.")
            continue

        image.flags.writeable = False
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        results = hands.process(image)

        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        count = 0
        if results.multi_hand_landmarks:
            for i, hand_landmarks in enumerate(results.multi_hand_landmarks):
                hand = results.multi_handedness[i].classification[0].label
                if (hand == "Right" and hand_landmarks.landmark[4].x < hand_landmarks.landmark[5].x):
                    count += 1
                if (hand == "Left" and hand_landmarks.landmark[4].x > hand_landmarks.landmark[5].x):
                    count += 1
                if (hand_landmarks.landmark[8].y < hand_landmarks.landmark[6].y):
                    count += 1
                if (hand_landmarks.landmark[12].y < hand_landmarks.landmark[10].y):
                    count += 1
                if (hand_landmarks.landmark[16].y < hand_landmarks.landmark[14].y):
                    count += 1
                if (hand_landmarks.landmark[20].y < hand_landmarks.landmark[18].y):
                    count += 1

                mp_drawing.draw_landmarks(
                    image,
                    hand_landmarks,
                    mp_hands.HAND_CONNECTIONS,
                    mp_drawing_styles.get_default_hand_landmarks_style(),
                    mp_drawing_styles.get_default_hand_connections_style())

        image = cv2.flip(image, 1)
        cv2.putText(image, str(count), (50, 50),
                    cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        cv2.imshow("Hands", image)

        if cv2.waitKey(5) & 0xFF == 27:
            break

cap.release()
